package nl.angarde.javabeg.exception.uitwerking;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * De FileImporterService importeert fictief een bestand van een verkeerde
 * lokatie om een Exception te krijgen. Het doel van deze service is het maken
 * van een goede foutafhandeling.
 * 
 * @author Andre
 *
 */
public class FileImporterServiceImpl {
	private static final Logger LOGGER = Logger.getLogger(FileImporterServiceImpl.class);

	/**
	 * Importeer een bestand met gegeven bestandsnaam.
	 * 
	 * @param bestandsNaam
	 *            Naam van het bestand dat ge�mporteerd moet worden.
	 * 
	 */
	public void importeerBestand(String bestandsNaam) throws IllegalArgumentException, FileNotFoundException {
		if (bestandsNaam == null) {
			throw new IllegalArgumentException("bestandsNaam kan niet leeg zijn!");
		}

		File importBestand = new File(bestandsNaam);

		printOutput(importBestand);

	}

	private void printOutput(File importBestand) throws FileNotFoundException {
		BufferedReader br = new BufferedReader(new FileReader(importBestand));

		try {
			String line;
			while ((line = br.readLine()) != null) {
				LOGGER.debug(line);
			}

		} catch (IOException ioException) {
			throw new RuntimeException("Importeren is niet gelukt.");
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				LOGGER.error("Kan bestand niet sluiten", e);
			}
		}

	}

}
